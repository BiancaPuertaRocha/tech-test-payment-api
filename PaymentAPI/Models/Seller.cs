﻿using System.ComponentModel.DataAnnotations;
namespace PaymentAPI.Models
{
    public class Seller
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        [Required]
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }



    }
}
