﻿namespace PaymentAPI.Models
{
    public class Sale
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public List<Item> Items { get; set; } = new List<Item>();
        public Seller Seller { get; set; } = null;

    }
}
