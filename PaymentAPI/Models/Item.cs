﻿using System.ComponentModel.DataAnnotations;


namespace PaymentAPI.Models
{
    public class Item
    {

        [Key]
        public int Id { get; set; }
        [Key]
        public int SaleId { get; set; }
        public Sale Sale { get; set; } = new Sale();
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

    }
}
