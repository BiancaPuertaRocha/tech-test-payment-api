﻿namespace PaymentAPI.Models
{
    public class ItemPK
    {
        public int Id { get; set; }
        public Sale Sale { get; set; } = new Sale();
    }
}
