using Microsoft.EntityFrameworkCore;
using PaymentAPI.Models;

namespace PaymentAPI{
    public class AppDbContext : DbContext
    {
        //table representation
        public DbSet<Sale>? Sales { get; set; }
        public DbSet<Seller>? Sellers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(connectionString:"DataSource=app.db;Cache=Shared");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Item>().HasKey(i => new { i.Id, i.SaleId });
        }
    }
}